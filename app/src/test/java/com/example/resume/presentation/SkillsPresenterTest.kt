package com.example.resume.presentation

import com.example.resume.domain.model.Skill
import com.example.resume.domain.usecase.GetSkillsUseCase
import com.example.resume.domain.usecase.SingleUseCase
import com.example.resume.presentation.ui.skills.SkillType
import com.example.resume.presentation.ui.skills.SkillsContract
import com.example.resume.presentation.ui.skills.SkillsPresenter
import io.mockk.*
import io.reactivex.Single
import org.junit.jupiter.api.Test

/**
 * @author Paweł Helm
 * @since 18/09/2019
 */

class SkillsPresenterTest {


    private val skillsPresenter: SkillsPresenter
    private val mockSkillsView: SkillsContract.View = mockk(relaxUnitFun = true)
    private val mockGetSkillsUseCase: GetSkillsUseCase = mockk()

    private val mockSkillsReturnData = listOf(Skill(5, "Java"))


    init {
        skillsPresenter = SkillsPresenter(
            mockSkillsView,
            mockGetSkillsUseCase
        )

        every { mockSkillsView.getSkillType() } returns SkillType.Programming
    }

    @Test
    fun `presenter inits view and executes usecase`() {
        every {
            mockGetSkillsUseCase.execute(
                any(),
                any()
            )
        } returns Single.error<List<Skill>>(IllegalStateException()).test()

        skillsPresenter.init()

        verifyOrder {
            mockSkillsView.initView()
            mockGetSkillsUseCase.execute(any(), any())
        }
    }

    @Test
    fun `presenter gets skills list onSuccess`() {
        val slot = slot<SingleUseCase.Subscriber<List<Skill>>>()
        val presenterSpy = spyk(skillsPresenter)
        val mockParam = SkillType.Programming

        every { mockGetSkillsUseCase.execute(capture(slot), mockParam) } returns Single.just(mockParam).test()

        presenterSpy.init()
        slot.captured.onSuccess(mockSkillsReturnData)

        verify(exactly = 1) {
            mockSkillsView.showSkills(mockSkillsReturnData)
        }
    }


    @Test
    fun `presenter gets skills list onError`() {
        val mockParam = SkillType.Programming

        val slot = slot<SingleUseCase.Subscriber<List<Skill>>>()
        every { mockGetSkillsUseCase.execute(capture(slot), mockParam) } returns Single.error<List<Skill>>(
            IllegalStateException()
        ).test()

        skillsPresenter.init()

        slot.captured.onError(IllegalStateException())
        verify(exactly = 1) {
            mockSkillsView.displayErrorDialog(any(), any())
        }
    }
}
