package com.example.resume.presentation

import com.example.resume.domain.model.Experience
import com.example.resume.domain.usecase.GetExperienceUseCase
import com.example.resume.domain.usecase.SingleUseCase
import com.example.resume.presentation.ui.experience.ExperienceContract
import com.example.resume.presentation.ui.experience.ExperiencePresenter
import com.example.resume.presentation.ui.experience.ExperienceType
import io.mockk.*
import io.reactivex.Single
import org.junit.jupiter.api.Test

/**
 * @author Paweł Helm
 * @since 17/09/2019
 */


class ExperiencePresenterTest {


    private val experiencePresenter: ExperiencePresenter
    private val mockExperienceView: ExperienceContract.View = mockk(relaxUnitFun = true)
    private val mockGetExperienceUseCase: GetExperienceUseCase = mockk()

    private val mockProfessionalReturnData = listOf(Experience("10.2016", "", "10.2019", "", "Cracow", ""))

    init {
        experiencePresenter = ExperiencePresenter(
            mockExperienceView,
            mockGetExperienceUseCase
        )

        every { mockExperienceView.getExperienceType() } returns ExperienceType.Professional
    }

    @Test
    fun `presenter inits view and executes usecase`(){
        every { mockGetExperienceUseCase.execute(any(), any()) } returns Single.error<List<Experience>>(IllegalStateException()).test()

        experiencePresenter.init()

        verifyOrder {
            mockExperienceView.initView()
            mockGetExperienceUseCase.execute(any(), any())
        }
    }

    @Test
    fun `presenter gets experience list onSuccess`() {
        val slot = slot<SingleUseCase.Subscriber<List<Experience>>>()
        val presenterSpy = spyk(experiencePresenter)
        val mockParam = ExperienceType.Professional

        every { mockGetExperienceUseCase.execute(capture(slot), mockParam) } returns Single.just(mockParam).test()

        presenterSpy.init()
        slot.captured.onSuccess(mockProfessionalReturnData)

        verify (exactly = 1) {
            mockExperienceView.showExperience(mockProfessionalReturnData)
        }
    }


    @Test
    fun `presenter gets experience list onError`() {
        val mockParam = ExperienceType.Professional

        val slot = slot<SingleUseCase.Subscriber<List<Experience>>>()
        every { mockGetExperienceUseCase.execute(capture(slot), mockParam) } returns Single.error<List<Experience>>(IllegalStateException()).test()

        experiencePresenter.init()

        slot.captured.onError(IllegalStateException())
        verify(exactly = 1) {
            mockExperienceView.displayErrorDialog(any(), any())
        }
    }
}