package com.example.resume.domain

import com.example.resume.domain.model.Experience
import com.example.resume.domain.usecase.GetExperienceUseCase
import com.example.resume.presentation.ui.experience.ExperienceType
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.jupiter.api.Test

/**
 * @author Paweł Helm
 * @since 17/09/2019
 */


class GetExperienceUseCaseTest {

    private val getExperienceUseCase: GetExperienceUseCase
    private val mockResumeRepository: ResumeRepository = mockk()

    private val mockProfessionalReturnData = listOf(Experience("10.2016", "", "10.2019", "", "Cracow", ""))
    private val mockEducationReturnData = listOf(Experience("", "", "", "", "", ""))

    init {
        getExperienceUseCase = GetExperienceUseCase(mockResumeRepository)
        every { mockResumeRepository.getExperience() } returns Single.just(mockProfessionalReturnData)
        every { mockResumeRepository.getEducation() } returns Single.just(mockEducationReturnData)
    }


    @Test
    fun `usecase calls repository for professional experience`() {
        val mockParam = ExperienceType.Professional
        getExperienceUseCase.createSingleUseCase(mockParam).test()
        verify(exactly = 1) {
            mockResumeRepository.getExperience()
        }
    }

    @Test
    fun `usecase returns professional experience data`() {
        val mockParam = ExperienceType.Professional
        val testObserver = getExperienceUseCase.createSingleUseCase(mockParam).test()
        testObserver.assertValue(mockProfessionalReturnData)
    }

    @Test
    fun `usecase calls repository for education`() {
        val mockParam = ExperienceType.Education
        getExperienceUseCase.createSingleUseCase(mockParam).test()
        verify(exactly = 1) {
            mockResumeRepository.getEducation()
        }
    }

    @Test
    fun `usecase returns education data`() {
        val mockParam = ExperienceType.Education
        val testObserver = getExperienceUseCase.createSingleUseCase(mockParam).test()
        testObserver.assertValue(mockEducationReturnData)
    }

    @Test
    fun `usecase completes`() {
        val mockParam = ExperienceType.Education
        val testObserver = getExperienceUseCase.createSingleUseCase(mockParam).test()
        testObserver.assertComplete()
    }

}