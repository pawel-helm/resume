package com.example.resume.domain

import com.example.resume.domain.model.Skill
import com.example.resume.domain.usecase.GetSkillsUseCase
import com.example.resume.presentation.ui.skills.SkillType
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.jupiter.api.Test

/**
 * @author Paweł Helm
 * @since 17/09/2019
 */

class GetSkillsUseCaseTest {

    private val getSkillsUseCase: GetSkillsUseCase
    private val mockResumeRepository: ResumeRepository = mockk()

    private val mockReturnProgrammingData = listOf(Skill(5, "Java"))
    private val mockReturnLanguageData = listOf(Skill(4, "English"))
    private val mockReturnLInterestsData = listOf(Skill(3, "Cycling"))

    init {
        getSkillsUseCase = GetSkillsUseCase(mockResumeRepository)
        every { mockResumeRepository.getProgrammingSkills() } returns Single.just(mockReturnProgrammingData)
        every { mockResumeRepository.getLanguages() } returns Single.just(mockReturnLanguageData)
        every { mockResumeRepository.getInterests() } returns Single.just(mockReturnLInterestsData)
    }


    @Test
    fun `usecase calls repository for programming skills`() {
        val mockParam = SkillType.Programming
        getSkillsUseCase.createSingleUseCase(mockParam).test()
        verify(exactly = 1) {
            mockResumeRepository.getProgrammingSkills()
        }
    }

    @Test
    fun `usecase returns programming skills data`() {
        val mockParam = SkillType.Programming
        val testObserver = getSkillsUseCase.createSingleUseCase(mockParam).test()
        testObserver.assertValue(mockReturnProgrammingData)
    }

    @Test
    fun `usecase calls repository for language skills`() {
        val mockParam = SkillType.Language
        getSkillsUseCase.createSingleUseCase(mockParam).test()
        verify(exactly = 1) {
            mockResumeRepository.getLanguages()
        }
    }

    @Test
    fun `usecase returns language skills data`() {
        val mockParam = SkillType.Language
        val testObserver = getSkillsUseCase.createSingleUseCase(mockParam).test()
        testObserver.assertValue(mockReturnLanguageData)
    }

    @Test
    fun `usecase calls repository for interests`() {
        val mockParam = SkillType.Interest
        getSkillsUseCase.createSingleUseCase(mockParam).test()
        verify(exactly = 1) {
            mockResumeRepository.getInterests()
        }
    }

    @Test
    fun `usecase returns interest data`() {
        val mockParam = SkillType.Interest
        val testObserver = getSkillsUseCase.createSingleUseCase(mockParam).test()
        testObserver.assertValue(mockReturnLInterestsData)
    }

    @Test
    fun `usecase completes`() {
        val mockParam = SkillType.Programming
        val testObserver = getSkillsUseCase.createSingleUseCase(mockParam).test()
        testObserver.assertComplete()
    }

}