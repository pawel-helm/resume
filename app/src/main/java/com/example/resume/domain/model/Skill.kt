package com.example.resume.domain.model

/**
 * @author Paweł Helm
 * @since 15/09/2019
 */

data class Skill(
    val level: Int,
    val name: String
)