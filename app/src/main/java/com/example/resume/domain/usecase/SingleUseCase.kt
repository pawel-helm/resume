package com.example.resume.domain.usecase

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */



import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


abstract class SingleUseCase<T, in Params> {

    /**
     * Builds a [Single] which will be used when the current [SingleUseCase] is executed.
     */
    abstract fun createSingleUseCase(params: Params? = null): Single<T>

    /**
     * Executes the current use case. Should be used only in Presenters
     */
    fun execute(subscriber: Subscriber<T>, params: Params? = null): Disposable {
        return this.createSingleUseCase(params)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { subscriber.doOnSubscribe() }
            .doFinally { subscriber.doFinally() }
            .subscribe(subscriber::onSuccess, subscriber::onError)
    }

    abstract class Subscriber<T> {
        abstract fun onSuccess(result: T)
        abstract fun onError(exp: Throwable)
        open fun doOnSubscribe() {}
        open fun doFinally() {}
    }
}