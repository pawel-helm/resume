package com.example.resume.domain.usecase

import com.example.resume.domain.ResumeRepository
import com.example.resume.domain.model.Experience
import com.example.resume.presentation.ui.experience.ExperienceType
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

class GetExperienceUseCase @Inject constructor(private val resumeRepository: ResumeRepository) :
    SingleUseCase<List<Experience>, ExperienceType?>() {

    override fun createSingleUseCase(params: ExperienceType?): Single<List<Experience>> {
        return when (params) {
            null -> throw IllegalArgumentException("null param")
            ExperienceType.Professional -> resumeRepository.getExperience()
            ExperienceType.Education -> resumeRepository.getEducation()
        }
    }
}