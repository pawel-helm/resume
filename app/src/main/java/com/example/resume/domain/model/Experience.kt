package com.example.resume.domain.model

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */


data class Experience(
    val startDate: String,
    val endDate: String,
    val title: String,
    val subtitle: String,
    val location: String,
    val description: String
)