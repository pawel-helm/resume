package com.example.resume.domain

import com.example.resume.domain.model.Experience
import com.example.resume.domain.model.Skill
import io.reactivex.Single

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

interface ResumeRepository {

    fun getExperience(): Single<List<Experience>>
    fun getEducation(): Single<List<Experience>>
    fun getProgrammingSkills(): Single<List<Skill>>
    fun getLanguages(): Single<List<Skill>>
    fun getInterests(): Single<List<Skill>>
}