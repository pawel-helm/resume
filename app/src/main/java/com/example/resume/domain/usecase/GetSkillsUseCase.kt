package com.example.resume.domain.usecase

import com.example.resume.domain.ResumeRepository
import com.example.resume.domain.model.Skill
import com.example.resume.presentation.ui.skills.SkillType
import io.reactivex.Single
import java.lang.IllegalArgumentException
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 16/09/2019
 */

class GetSkillsUseCase @Inject constructor(private val resumeRepository: ResumeRepository) : SingleUseCase<List<Skill>, SkillType?>() {

    override fun createSingleUseCase(params: SkillType?): Single<List<Skill>> {
        return when (params) {
            null -> throw IllegalArgumentException("null param")
            SkillType.Programming -> resumeRepository.getProgrammingSkills()
            SkillType.Language -> resumeRepository.getLanguages()
            SkillType.Interest -> resumeRepository.getInterests()
        }
    }
}