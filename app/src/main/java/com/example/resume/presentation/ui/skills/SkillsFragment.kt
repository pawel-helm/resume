package com.example.resume.presentation.ui.skills

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.resume.R
import com.example.resume.domain.model.Skill
import com.example.resume.presentation.navigation.Navigator
import com.example.resume.presentation.ui.BaseFragment
import kotlinx.android.synthetic.main.skills_fragment.*
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 15/09/2019
 */


class SkillsFragment : BaseFragment<SkillsContract.Presenter>(), SkillsContract.View {

    companion object {
        private const val ARG_SKILL_TYPE = "SKILL_TYPE"

        fun newInstance(skillType: SkillType): SkillsFragment = SkillsFragment().apply {
            val bundle = Bundle()
            bundle.putParcelable(ARG_SKILL_TYPE, skillType)
            arguments = bundle
        }
    }

    @Inject
    override lateinit var presenter: SkillsContract.Presenter

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var adapter: SkillsAdapter

    override fun getLayoutResource() = R.layout.skills_fragment

    override fun getIdent() = "Skills"

    override fun initView() {
        getSkillType()?.let {
            baseActivity.setStatusBarColor(it.mainColor)
            flBackground.setBackgroundColor(ContextCompat.getColor(requireContext(), it.mainColor))
            toolbar.setBackgroundColor(ContextCompat.getColor(requireContext(), it.mainColor))
            tvToolbarTitle.text = getText(it.toolbarTitle)
            toolbar.transitionName = getString(it.transitionName)
            tvSubtitle.text = getString(it.subtitle)
        }

        rvSkills.adapter = adapter
        rvSkills.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun getSkillType(): SkillType? = arguments?.getParcelable(ARG_SKILL_TYPE)

    override fun showSkills(skills: List<Skill>) {
        adapter.items = skills
    }
}