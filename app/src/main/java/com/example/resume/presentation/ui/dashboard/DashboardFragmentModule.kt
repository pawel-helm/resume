package com.example.resume.presentation.ui.dashboard

import dagger.Module
import dagger.Provides

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */
@Module
class DashboardFragmentModule {

    @Provides
    fun provideDashboardView(DashboardFragment: DashboardFragment): DashboardContract.View = DashboardFragment

    @Provides
    fun provideDashboardPresenter(view: DashboardContract.View): DashboardContract.Presenter = DashboardPresenter(view)
}