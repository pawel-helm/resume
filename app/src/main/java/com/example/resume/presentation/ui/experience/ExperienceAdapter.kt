package com.example.resume.presentation.ui.experience

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.resume.R
import javax.inject.Inject
import com.example.resume.domain.model.Experience
import kotlinx.android.synthetic.main.experience_row_item.*


/**
 * @author Paweł Helm
 * @since 15/09/2019
 */


class ExperienceAdapter @Inject constructor() : RecyclerView.Adapter<ExperienceViewHolder>() {

    companion object {
        private const val VIEW_TYPE_TOP = 0
        private const val VIEW_TYPE_MIDDLE = 1
        private const val VIEW_TYPE_BOTTOM = 2
    }

    var items: List<Experience> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExperienceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ExperienceViewHolder(inflater.inflate(ExperienceViewHolder.LAYOUT_ID, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ExperienceViewHolder, position: Int) {
        holder.bind(items[position])
        when (holder.itemViewType) {
            VIEW_TYPE_TOP ->
                holder.line.background = ContextCompat.getDrawable(holder.containerView.context, R.drawable.line_bg_top)
            VIEW_TYPE_MIDDLE ->
                holder.line.background = ContextCompat.getDrawable(holder.containerView.context, R.drawable.line_bg_middle)
            VIEW_TYPE_BOTTOM -> holder.line.background = ContextCompat.getDrawable(holder.containerView.context, R.drawable.line_bg_bottom)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> VIEW_TYPE_TOP
            items.size -1 -> VIEW_TYPE_BOTTOM
            else -> VIEW_TYPE_MIDDLE
        }
    }
}
