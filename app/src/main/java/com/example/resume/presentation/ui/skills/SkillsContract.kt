package com.example.resume.presentation.ui.skills

import com.example.resume.domain.model.Skill
import com.example.resume.presentation.ui.BaseContract

/**
 * @author Paweł Helm
 * @since 15/09/2019
 */
interface SkillsContract {


    interface View : BaseContract.View {
        fun initView()
        fun showSkills(skills: List<Skill>)
        fun getSkillType(): SkillType?
    }

    interface Presenter : BaseContract.Presenter {

    }

}