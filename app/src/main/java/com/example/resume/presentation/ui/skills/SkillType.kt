package com.example.resume.presentation.ui.skills

import android.os.Parcelable
import com.example.resume.R
import kotlinx.android.parcel.Parcelize

/**
 * @author Paweł Helm
 * @since 16/09/2019
 */


sealed class SkillType(val mainColor: Int, val transitionName: Int, val toolbarTitle: Int, val subtitle: Int): Parcelable{

    @Parcelize
    object Programming: SkillType(R.color.red, R.string.transition_name_skills, R.string.title_skills, R.string.skills_subtitle)

    @Parcelize
    object Language: SkillType(R.color.green, R.string.transition_name_language, R.string.title_languages, R.string.title_languages)

    @Parcelize
    object Interest: SkillType(R.color.blue, R.string.transition_name_interest, R.string.title_interests, R.string.sport_subtitle)
}