package com.example.resume.presentation.ui.experience

import com.example.resume.domain.usecase.SingleUseCase
import com.example.resume.domain.model.Experience
import com.example.resume.domain.usecase.GetExperienceUseCase
import com.example.resume.presentation.ui.BasePresenter
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

class ExperiencePresenter @Inject constructor(val view: ExperienceContract.View,
                                              private val getExperienceUseCase: GetExperienceUseCase)
    : BasePresenter(), ExperienceContract.Presenter {


    override fun init() {
        super.init()
        view.initView()
        getExperience()   
    }

    private fun getExperience() {
        getExperienceUseCase.execute(GetExperienceSubscriber(), view.getExperienceType()).addPersistDisposable()
    }
    
    private inner class GetExperienceSubscriber : SingleUseCase.Subscriber<List<Experience>>() {
            override fun onSuccess(result: List<Experience>) {
                view.showExperience(result)
            }
    
            override fun onError(exp: Throwable) {
                view.displayErrorDialog(exp) { getExperience() }
            }
    }
}
