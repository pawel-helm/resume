package com.example.resume.presentation.ui.main

import com.example.resume.presentation.ui.dashboard.DashboardFragment
import com.example.resume.presentation.ui.dashboard.DashboardFragmentModule
import com.example.resume.presentation.ui.experience.ExperienceFragment
import com.example.resume.presentation.ui.experience.ExperienceFragmentModule
import com.example.resume.presentation.ui.skills.SkillsFragment
import com.example.resume.presentation.ui.skills.SkillsFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

@Module
abstract class MainFragmentsProvider {

    @ContributesAndroidInjector(modules = arrayOf(DashboardFragmentModule::class))
    abstract fun provideDashboardFragmentFactory(): DashboardFragment

    @ContributesAndroidInjector(modules = arrayOf(ExperienceFragmentModule::class))
    abstract fun provideExperienceFragmentFactory(): ExperienceFragment

    @ContributesAndroidInjector(modules = arrayOf(SkillsFragmentModule::class))
    abstract fun provideSkillsFragmentFactory(): SkillsFragment
}