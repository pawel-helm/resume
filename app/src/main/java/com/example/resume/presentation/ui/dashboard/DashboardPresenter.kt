package com.example.resume.presentation.ui.dashboard

import com.example.resume.presentation.ui.BasePresenter
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

class DashboardPresenter @Inject constructor(val view: DashboardContract.View) : BasePresenter(),
    DashboardContract.Presenter {


    override fun init() {
        super.init()
        view.initView()
    }

    override fun onExperienceClick() {
        view.showExperince()
    }

    override fun onEducationClick() {
        view.showEducation()
    }

    override fun onSkillsClick() {
        view.showSkills()
    }

    override fun onLanguagesClick() {
        view.showLanguages()
    }

    override fun onInterestsClick() {
        view.showInterests()
    }
}
