package com.example.resume.presentation.ui.skills

import com.example.resume.domain.usecase.SingleUseCase
import com.example.resume.domain.model.Skill
import com.example.resume.domain.usecase.GetSkillsUseCase
import com.example.resume.presentation.ui.BasePresenter
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 15/09/2019
 */

class SkillsPresenter @Inject constructor(val view: SkillsContract.View, private val getSkillsUseCase: GetSkillsUseCase)
    : BasePresenter(), SkillsContract.Presenter {


    override fun init() {
        super.init()
        view.initView()
        getSkills()
    }

    private fun getSkills(){
        getSkillsUseCase.execute(GetSkillsUseCaseSubscriber(), view.getSkillType()).addPersistDisposable()
    }

    private inner class GetSkillsUseCaseSubscriber : SingleUseCase.Subscriber<List<Skill>>() {
            override fun onSuccess(result: List<Skill>) {
                view.showSkills(result)
            }

            override fun onError(exp: Throwable) {
                view.displayErrorDialog(exp) { getSkills() }
            }
    }
}

