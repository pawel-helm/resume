package com.example.resume.presentation.navigation

import android.view.View
import com.example.resume.presentation.ui.BaseActivity
import com.example.resume.presentation.ui.dashboard.DashboardFragment
import com.example.resume.presentation.ui.experience.ExperienceFragment
import com.example.resume.presentation.ui.experience.ExperienceType
import com.example.resume.presentation.ui.skills.SkillType
import com.example.resume.presentation.ui.skills.SkillsFragment

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */


class Navigator {

    fun showDashboardFragment(activity: BaseActivity, resetStack: Boolean = false) {
        activity.showFragment(DashboardFragment.newInstance(), resetStack)
    }

    fun showExperienceFragment(
        activity: BaseActivity,
        sharedElement: View? = null,
        transitionName: String? = null,
        resetStack: Boolean = false,
        experience: ExperienceType
    ) {
        activity.showFragmentWithSharedElement(
            DashboardFragment.newInstance(),
            ExperienceFragment.newInstance(experience),
            sharedElement,
            transitionName,
            resetStack
        )
    }


    fun showSkillsFragment(
        activity: BaseActivity,
        sharedElement: View? = null,
        transitionName: String? = null,
        resetStack: Boolean = false,
        skillType: SkillType
    ) {
        activity.showFragmentWithSharedElement(
            DashboardFragment.newInstance(),
            SkillsFragment.newInstance(skillType),
            sharedElement,
            transitionName,
            resetStack
        )
    }

}