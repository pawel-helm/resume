package com.example.resume.presentation.ui.skills

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.resume.R
import com.example.resume.domain.model.Skill
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.skill_row_item.*

/**
 * @author Paweł Helm
 * @since 16/09/2019
 */

class SkillsViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    companion object {
        const val LAYOUT_ID = R.layout.skill_row_item
    }

    fun bind(model: Skill) {
        tvSkill.text = model.name
        ratingBar.rating = model.level.toFloat()
    }

}