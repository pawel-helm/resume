package com.example.resume.presentation.ui.experience

import com.example.resume.domain.model.Experience
import com.example.resume.presentation.ui.BaseContract

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */
interface ExperienceContract {


    interface View : BaseContract.View {
        fun initView()
        fun showExperience(list: List<Experience>)
        fun getExperienceType(): ExperienceType?
    }

    interface Presenter : BaseContract.Presenter {

    }

}