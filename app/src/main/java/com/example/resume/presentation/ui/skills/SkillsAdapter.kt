package com.example.resume.presentation.ui.skills

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.resume.R
import com.example.resume.domain.model.Experience
import com.example.resume.domain.model.Skill
import com.example.resume.presentation.ui.experience.ExperienceViewHolder
import kotlinx.android.synthetic.main.experience_row_item.*
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 16/09/2019
 */


class SkillsAdapter @Inject constructor() : RecyclerView.Adapter<SkillsViewHolder>() {

    var items: List<Skill> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SkillsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SkillsViewHolder(inflater.inflate(SkillsViewHolder.LAYOUT_ID, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SkillsViewHolder, position: Int) {
        holder.bind(items[position])
    }
}