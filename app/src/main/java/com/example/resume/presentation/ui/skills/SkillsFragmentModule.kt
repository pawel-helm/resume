package com.example.resume.presentation.ui.skills

import com.example.resume.domain.usecase.GetSkillsUseCase
import dagger.Module
import dagger.Provides

/**
 * @author Paweł Helm
 * @since 15/09/2019
 */
@Module
class SkillsFragmentModule {

    @Provides
    fun provideSkillsView(SkillsFragment: SkillsFragment): SkillsContract.View = SkillsFragment

    @Provides
    fun provideSkillsPresenter(
        view: SkillsContract.View,
        getSkillsUseCase: GetSkillsUseCase
    ): SkillsContract.Presenter = SkillsPresenter(view, getSkillsUseCase)
}