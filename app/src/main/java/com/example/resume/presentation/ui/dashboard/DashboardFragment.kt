package com.example.resume.presentation.ui.dashboard

import com.example.resume.R
import com.example.resume.presentation.navigation.Navigator
import com.example.resume.presentation.ui.BaseFragment
import com.example.resume.presentation.ui.experience.ExperienceType
import com.example.resume.presentation.ui.skills.SkillType
import javax.inject.Inject
import kotlinx.android.synthetic.main.dashboard_fragment.*


/**
 * @author Paweł Helm
 * @since 14/09/2019
 */


class DashboardFragment : BaseFragment<DashboardContract.Presenter>(), DashboardContract.View {

    companion object {
        fun newInstance() = DashboardFragment()
    }

    @Inject
    override lateinit var presenter: DashboardContract.Presenter

    @Inject
    lateinit var navigator: Navigator

    override fun getLayoutResource() = R.layout.dashboard_fragment

    override fun getIdent() = "Dashboard"

    override fun initView() {
        baseActivity.setStatusBarColor(R.color.colorPrimary)

        tvExperience.setOnClickListener { presenter.onExperienceClick() }
        tvEducation.setOnClickListener { presenter.onEducationClick() }
        tvSkills.setOnClickListener { presenter.onSkillsClick() }
        tvLanguages.setOnClickListener { presenter.onLanguagesClick() }
        tvInterests.setOnClickListener { presenter.onInterestsClick() }
    }

    override fun showExperince() {
        navigator.showExperienceFragment(baseActivity, vgExperience, vgExperience.transitionName, false, ExperienceType.Professional)
    }

    override fun showEducation() {
        navigator.showExperienceFragment(baseActivity, vgEducation, vgEducation.transitionName, false, ExperienceType.Education)
    }

    override fun showSkills() {
        navigator.showSkillsFragment(baseActivity, vgSkills, vgSkills.transitionName,false, SkillType.Programming)
    }

    override fun showLanguages() {
        navigator.showSkillsFragment(baseActivity, vgLanguages, vgLanguages.transitionName,false, SkillType.Language)
    }

    override fun showInterests() {
        navigator.showSkillsFragment(baseActivity, vgInterests, vgInterests.transitionName,false, SkillType.Interest)
    }
}