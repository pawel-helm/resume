package com.example.resume.presentation.navigation

import android.view.View
import androidx.fragment.app.FragmentActivity
import com.example.resume.R
import com.example.resume.presentation.ui.BaseContract
import com.example.resume.presentation.ui.BaseFragment

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

fun FragmentActivity.showFragment(
    fragment: BaseFragment<*>,
    resetStack: Boolean = false,
    forceSameFragment: Boolean = false
) {

    if (!forceSameFragment) {
        if (displayFragmentFromStackIfPresent(fragment.getIdent())) {
            return
        }
    }

    if (resetStack) {
        supportFragmentManager.popBackStack(null, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    val fragmentTransaction: androidx.fragment.app.FragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.replace(R.id.vgBaseFragmentContainer, fragment, fragment.getIdent())
    fragmentTransaction.addToBackStack(fragment.getIdent())

    if (!isFinishing) {
        fragmentTransaction.commit()
    }
}

fun FragmentActivity.displayFragmentFromStackIfPresent(ident: String): Boolean {
    if (supportFragmentManager.backStackEntryCount > 0) {
        for (i in 0 until supportFragmentManager.backStackEntryCount) {
            val fragmentName = supportFragmentManager.getBackStackEntryAt(i).name
            val fragment = supportFragmentManager.findFragmentByTag(fragmentName)
            if ((fragment as BaseContract.View).getIdent() == ident) {
                supportFragmentManager.popBackStack(ident, 0)
                return true
            }
        }
    }
    return false
}

fun FragmentActivity.showFragmentWithSharedElement(
    oldFragment: BaseFragment<*>,
    newFragment: BaseFragment<*>,
    sharedElement: View?,
    transitionName: String?,
    resetStack: Boolean = false
) {

    if (resetStack) {
        try {
            supportFragmentManager.popBackStack(null, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
        } catch (e: IllegalStateException) {
            //ignore
        }
    }

    val changeTransform =
        android.transition.TransitionInflater.from(this).inflateTransition(R.transition.shared_element_transition)

    oldFragment.sharedElementReturnTransition = changeTransform
    newFragment.sharedElementEnterTransition = changeTransform

    val fragmentTransaction: androidx.fragment.app.FragmentTransaction = supportFragmentManager.beginTransaction()
    if (sharedElement != null && transitionName != null) {
        fragmentTransaction.addSharedElement(sharedElement, transitionName)
    }
    fragmentTransaction.replace(R.id.vgBaseFragmentContainer, newFragment, newFragment.getIdent())
    fragmentTransaction.addToBackStack(newFragment.getIdent())

    if (!isFinishing) {
        fragmentTransaction.commitAllowingStateLoss()
    }
}
