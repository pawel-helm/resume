package com.example.resume.presentation.ui.experience

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.resume.R
import com.example.resume.domain.model.Experience
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.experience_row_item.*

/**
 * @author Paweł Helm
 * @since 15/09/2019
 */


class ExperienceViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {


    companion object {
        const val LAYOUT_ID = R.layout.experience_row_item
    }

    fun bind(model: Experience) {
        tvDate.text = String.format(tvDate.context.getString(R.string.common_from_to), model.startDate, model.endDate)
        tvTitle.text = model.title
        tvSubtitle.text = model.subtitle
        tvDescription.text = model.description
    }

}
