package com.example.resume.presentation.ui

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

interface BaseContract {

    interface Presenter {
        fun init()
        fun start()
        fun resume()
        fun pause()
        fun stop()
        fun destroyView()
        fun destroy()
    }

    interface View {
        fun getLayoutResource(): Int
        fun getIdent(): String
        fun displayErrorDialog(error: Throwable, retryAction: (() -> Unit))
    }

}
