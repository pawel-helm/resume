package com.example.resume.presentation.ui.dashboard

import com.example.resume.presentation.ui.BaseContract


/**
 * @author Paweł Helm
 * @since 14/09/2019
 */
interface DashboardContract {


    interface View : BaseContract.View {
        fun initView()
        fun showExperince()
        fun showEducation()
        fun showSkills()
        fun showLanguages()
        fun showInterests()
    }

    interface Presenter : BaseContract.Presenter {
        fun onExperienceClick()
        fun onEducationClick()
        fun onSkillsClick()
        fun onLanguagesClick()
        fun onInterestsClick()
    }

}