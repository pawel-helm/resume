package com.example.resume.presentation.ui

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.example.resume.R
import com.github.ajalt.timberkt.e
import dagger.android.support.DaggerFragment


abstract class BaseFragment<out T : BaseContract.Presenter> : DaggerFragment(),
    BaseContract.View {

    abstract val presenter: T

    protected lateinit var baseActivity: BaseActivity

    private var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseActivity = activity as BaseActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutResource(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.init()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onResume() {
        super.onResume()
        presenter.resume()
    }

    override fun onPause() {
        super.onPause()
        presenter.pause()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.destroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
        hideDialog()
    }

    override fun displayErrorDialog(error: Throwable, retryAction: (() -> Unit)) {
        e { "displayErrorDialog() \nstack: ${error.stackTrace} \nretry: ${retryAction.javaClass}" }

        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.error)
            .setMessage(R.string.error_dialog_message)
            .setPositiveButton(R.string.retry) { _, _ -> retryAction.invoke() }
            .setNegativeButton(R.string.cancel) { _, _ -> baseActivity.onBackPressed() }

        showDialog(dialog)
    }


    protected open fun showDialog(dialog: AlertDialog.Builder) {
        if (isAdded) {
            this.dialog = dialog.show()
        }
    }

    private fun hideDialog() {
        dialog?.run {
            if (isShowing) {
                dismiss()
            }
        }
    }
}

