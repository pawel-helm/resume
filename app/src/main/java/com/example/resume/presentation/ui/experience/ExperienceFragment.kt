package com.example.resume.presentation.ui.experience

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.resume.R
import com.example.resume.domain.model.Experience
import com.example.resume.presentation.navigation.Navigator
import com.example.resume.presentation.ui.BaseFragment
import kotlinx.android.synthetic.main.experience_fragment.*
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */


class ExperienceFragment : BaseFragment<ExperienceContract.Presenter>(), ExperienceContract.View {

    companion object {

        private const val ARG_EXPERIENCE_TYPE = "EXPERIENCE_TYPE"

        fun newInstance(experience: ExperienceType):ExperienceFragment = ExperienceFragment().apply {
            val bundle = Bundle()
            bundle.putParcelable(ARG_EXPERIENCE_TYPE, experience)
            arguments = bundle
        }
    }

    @Inject
    lateinit var adapter: ExperienceAdapter

    @Inject
    override lateinit var presenter: ExperienceContract.Presenter

    @Inject
    lateinit var navigator: Navigator

    override fun getLayoutResource() = R.layout.experience_fragment

    override fun getIdent() = "Professional"

    override fun initView() {
        baseActivity.setStatusBarColor(R.color.purple)

        getExperienceType()?.let {
            baseActivity.setStatusBarColor(it.mainColor)
            flBackground.setBackgroundColor(ContextCompat.getColor(requireContext(), it.mainColor))
            toolbar.setBackgroundColor(ContextCompat.getColor(requireContext(), it.mainColor))
            tvToolbarTitle.text = getText(it.toolbarTitle)
            toolbar.transitionName = getString(it.transitionName)
        }

        rvExperience.adapter = adapter
        rvExperience.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun getExperienceType(): ExperienceType? {
        return arguments?.getParcelable(ARG_EXPERIENCE_TYPE)
    }

    override fun showExperience(list: List<Experience>) {
        adapter.items = list
    }
}