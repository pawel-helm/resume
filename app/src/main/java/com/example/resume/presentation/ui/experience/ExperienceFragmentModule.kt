package com.example.resume.presentation.ui.experience

import com.example.resume.domain.usecase.GetExperienceUseCase
import dagger.Module
import dagger.Provides

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */
@Module
class ExperienceFragmentModule {

    @Provides
    fun provideExperienceView(ExperienceFragment: ExperienceFragment): ExperienceContract.View = ExperienceFragment

    @Provides
    fun provideExperiencePresenter(view: ExperienceContract.View, getExperienceUseCase: GetExperienceUseCase): ExperienceContract.Presenter =
        ExperiencePresenter(view, getExperienceUseCase)
}