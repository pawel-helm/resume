package com.example.resume.presentation.ui.experience

import android.os.Parcelable
import com.example.resume.R
import kotlinx.android.parcel.Parcelize

/**
 * @author Paweł Helm
 * @since 17/09/2019
 */

sealed class ExperienceType(val mainColor: Int, val transitionName: Int, val toolbarTitle: Int) : Parcelable {

    @Parcelize
    object Professional : ExperienceType(R.color.purple, R.string.transition_name_experience, R.string.title_professional_experience)

    @Parcelize
    object Education : ExperienceType(R.color.orange, R.string.transition_name_education, R.string.title_education)

}