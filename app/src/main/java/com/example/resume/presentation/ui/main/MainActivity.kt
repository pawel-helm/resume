package com.example.resume.presentation.ui.main

import android.os.Bundle
import com.example.resume.R
import com.example.resume.presentation.navigation.Navigator
import com.example.resume.presentation.ui.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var navigator: Navigator

    override fun getLayoutResource() = R.layout.main_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator.showDashboardFragment(this, false)
    }
}
