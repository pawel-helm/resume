package com.example.resume.injection

import android.app.Application
import android.content.Context
import com.example.resume.BuildConfig
import com.example.resume.data.config.NetworkServiceFactory
import com.example.resume.data.ResumeDataRepository
import com.example.resume.data.ResumeNetworkService
import com.example.resume.domain.ResumeRepository
import com.example.resume.presentation.navigation.Navigator
import dagger.Module
import dagger.Provides

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

@Module
open class RootApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PerApplication
    fun provideNavigator(): Navigator {
        return Navigator()
    }

    @Provides
    @PerApplication
    fun provideResumeNetworkService(networkServiceFactory: NetworkServiceFactory): ResumeNetworkService {
        return networkServiceFactory.makeRetrofitService(BuildConfig.URL_WEB_SERVICE)
            .create(ResumeNetworkService::class.java)
    }

    @Provides
    @PerApplication
    fun provideResumeRepository(networkService: ResumeNetworkService): ResumeRepository = ResumeDataRepository(networkService)

}