package com.example.resume.injection

import com.example.resume.presentation.ui.main.MainActivity
import com.example.resume.presentation.ui.main.MainFragmentsProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainFragmentsProvider::class])
    abstract fun bindDashboardActivity(): MainActivity

}