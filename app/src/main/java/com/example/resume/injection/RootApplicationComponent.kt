package com.example.resume.injection

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

import android.app.Application
import com.example.resume.RootApplication
import com.example.resume.data.config.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(
    modules = [
        (AndroidSupportInjectionModule::class),
        (NetworkModule::class),
        (RootApplicationModule::class),
        (ActivityBuilder::class)]
)

interface RootApplicationComponent : AndroidInjector<DaggerApplication> {


    fun inject(rootApp: RootApplication)

    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): RootApplicationComponent
    }

}