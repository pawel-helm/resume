package com.example.resume.injection

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication