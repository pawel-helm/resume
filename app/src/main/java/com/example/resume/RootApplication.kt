package com.example.resume

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */


import com.example.resume.injection.DaggerRootApplicationComponent
import com.github.ajalt.timberkt.Timber
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication


class RootApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerRootApplicationComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }


    override fun onCreate() {
        super.onCreate()
        Timber.plant(timber.log.Timber.DebugTree())
    }
}