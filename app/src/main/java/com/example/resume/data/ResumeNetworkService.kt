package com.example.resume.data

import com.example.resume.data.model.ExperienceResponse
import com.example.resume.data.model.SkillsResponse
import io.reactivex.Single
import retrofit2.http.GET

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */


interface ResumeNetworkService {

    @GET("bins/rntt5")
    fun getExperience(): Single<ExperienceResponse>

    @GET("bins/1ck555")
    fun getEducation(): Single<ExperienceResponse>

    @GET("bins/xh4gd")
    fun getProgrammingSkills(): Single<SkillsResponse>

    @GET("bins/qf6h9")
    fun getLanguages(): Single<SkillsResponse>

    @GET("bins/1gqyul")
    fun getInterests(): Single<SkillsResponse>

}