package com.example.resume.data.model

import com.example.resume.domain.model.Experience

/**
 * @author Paweł Helm
 * @since 15/09/2019
 */

data class ExperienceResponse(
    val experience: List<Experience>
)