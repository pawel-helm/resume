package com.example.resume.data

import com.example.resume.domain.ResumeRepository
import com.example.resume.domain.model.Experience
import com.example.resume.domain.model.Skill
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Paweł Helm
 * @since 14/09/2019
 */

class ResumeDataRepository @Inject constructor(private val networkService: ResumeNetworkService) : ResumeRepository {

    override fun getExperience(): Single<List<Experience>> =
        networkService.getExperience().map { it.experience }

    override fun getEducation(): Single<List<Experience>> =
        networkService.getEducation().map { it.experience }

    override fun getProgrammingSkills(): Single<List<Skill>> =
            networkService.getProgrammingSkills().map { it.skills }

    override fun getLanguages(): Single<List<Skill>> =
            networkService.getLanguages().map{ it.skills }

    override fun getInterests(): Single<List<Skill>> =
            networkService.getInterests().map { it.skills }

}