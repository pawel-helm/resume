package com.example.resume.data.model

import com.example.resume.domain.model.Skill

/**
 * @author Paweł Helm
 * @since 16/09/2019
 */

data class SkillsResponse(
    val skills: List<Skill>
)